<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>register</title>
</head>

<body>
<h1>Buat Account Baru</h1>
<h2>Sign Up Form</h2>
<form action="/welcome" method = "post">
    @csrf
    <label>First name:</label><br>
    <input type="text" name="fname"><br><br>
    <label>Last name:</label><br>
    <input type="text" name="lname"><br><br>
    
    <label>Gender: </label><br>
    <input type="radio" name="gender" value="Male">
    <label for="male">Male</label><br>
    <input type="radio" name="gender" value="Female">
    <label for="female">Female</label><br>
    <input type="radio" name="gender" value="Other">
    <label for="other">Other</label><br><br>
    
    <label>Nationality: </label>
    <select name="nationality"><br>
        <option value="indonesia" selected>Indonesian</option>
        <option value="singapore">Singapore</option>
        <option value="malaysia">Malaysia</option>
        <option value="australia">Australia</option>
    </select><br><br>
    
    <label>Language Spoken: </label><br>
    <input type="checkbox" name="bahasa1" value="bindo">
    <label for="bahasa1"> Bahasa Indonesia</label><br>
    <input type="checkbox" name="bahasa2" value="inggris">
    <label for="bahasa2"> English</label><br>
    <input type="checkbox" name="bahasa3" value="other">
    <label for="bahasa3"> Other</label><br><br>
    
    <label>Bio: </label><br>
    <textarea name="message" rows="10" cols="30"></textarea>
    <br>
    
    <input type="submit" value="Sign Up">

</form> 
</body>

</html>