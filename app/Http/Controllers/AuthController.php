<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registrasi(){
        return view('halaman.register');
    }

    public function welcome(Request $request){
        $fnama =$request['fname'];
        $lnama = $request['lname'];
        return view('halaman.home', compact('fnama', 'lnama'));
    }
}
